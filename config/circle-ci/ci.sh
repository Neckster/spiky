set -e
if [ -z "$CI_PULL_REQUEST" ]
then
  npm run node:cov
  cat coverage/lcov.info | node_modules/.bin/coveralls || echo "Coveralls upload failed"
else
  npm test
fi
