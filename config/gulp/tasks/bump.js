import gulp from 'gulp';
import bump from 'gulp-bump';
import { commit, push } from '../util/gitTasks';
import pkg from '../../../package.json';

gulp.task('bump', () => {
	gulp.src('./package.json')
		.pipe(bump())
		.pipe(gulp.dest('./'));
	// make a commit
	commit('bumped version to ' + pkg.version);
	// push to repo
	push();
});
