import gulp from 'gulp';
import runKarma from '../util/runKarma';

gulp.task('test:phantom', runKarma(true, {
		singleRun: true,
		browsers: ['PhantomJS']
	}
));